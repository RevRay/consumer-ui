package ru.devray.consumerui.models;

import lombok.Data;

import java.util.List;

@Data
public class ProductCatalogue {
  private final String name;
  private final List<Product> products;
}
