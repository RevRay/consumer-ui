package ru.devray.consumerui.clients;

import ru.devray.consumerui.models.Product;
import lombok.Data;

import java.util.List;

@Data
public class ProductServiceResponse {
  private List<Product> products;
}
